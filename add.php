<?php
    if($_SERVER["REQUEST_METHOD"]=== 'POST'){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "login";

        // Create connection
        $mysqli = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($mysqli->connect_error) {
        die("Connection failed: " . $mysqli->connect_error);
        }

        // $ID = $_POST['ID'];
        $FirstName = $_POST['FirstName'];
        $LastName = $_POST['LastName'];
        $Email = $_POST['Email'];

        $sql = "INSERT INTO users ( FirstName, LastName, Email)
        VALUES ('$FirstName','$LastName','$Email')";

        if ($mysqli->query($sql) === TRUE) {
        echo "New record created successfully";
        } else {
        echo "Error: " . $sql . "<br>" . $mysqli->error;
        }
        header( 'Location: /demo/user/index.php' );
        $mysqli->close();
    }
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="style.css?v=<?php echo time(); ?>">
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Add User</h2>
                    </div>
                    <p>Please fill this form and submit to add user.</p>
                    <form action="/demo/user/add.php" method="post">
                        <div class="form-group ">
                            <label>First Name</label>
                            <input type="text" name="FirstName" class="form-control" >
                        </div>
                        <div class="form-group ">
                            <label>Last Name</label>
                            <input type="text" name="LastName" class="form-control" >
                        </div>
                        <div class="form-group ">
                            <label>Email</label>
                            <input type="text" name="Email" class="form-control" >
                        </div>
                        <input type="hidden" name="ID" >
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>