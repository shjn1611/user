<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "login";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
 
if (count($_POST)>0) {
    mysqli_query($conn,"UPDATE users set Email='" . $_POST['Email'] . "', FirstName='" . $_POST['FirstName'] . "', LastName='" . $_POST['LastName'] . "' WHERE ID='" . $_POST['ID'] . "'");
    $message = "Record Modified Successfully";
    header( 'Location: /demo/user/index.php' );
}
$result = mysqli_query($conn,"SELECT * FROM users WHERE ID='" . $_GET['ID'] . "'");
$row= mysqli_fetch_array($result);
        
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="style.css?v=<?php echo time(); ?>">
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Update Record</h2>
                    </div>
                    <p>Please edit the input values and submit.</p>
                    <form action="/demo/user/edit.php" method="post">
                        <div class="form-group ">
                            <label>First Name</label>
                            <input type="text" name="FirstName" class="form-control" value="<?php echo $row['FirstName']; ?>">
                        </div>
                        <div class="form-group ">
                            <label>Last Name</label>
                            <input type="text" name="LastName" class="form-control" value="<?php echo $row['LastName']; ?>">
                        </div>
                        <div class="form-group ">
                            <label>Email</label>
                            <input type="text" name="Email" class="form-control" value="<?php echo $row['Email']; ?>">
                        </div>
                        <input type="hidden" name="ID" value="<?php echo $row['ID']; ?>"/>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>